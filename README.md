# Spatial APSIM - Indo-Gangetic Plains

This reposoitory contains the confguration files and modifed pSIMS (https://github.com/RDCEP/psims) for high-resolution spatial rice-wheat system simulations in the Indo-Gangetic Plains with the The Agricultural Production Systems sIMulator (APSIM; https://www.apsim.info/). 

The APSIM initiatives requires users to agree to the APSIM Terms and Conditions and obtain their own individual licence on this website: https://www.apsim.info/download-apsim/. 

The pSIMS project is lincensed under the Affero GPL lincense and reuse of pSIMS code requires to strictly follow the guidelines of that specific license.

This work builds on previous work and data collection efforts, especially by Balwinder-Singh from CIMMYT International Maize and Wheat Improve and Donald Gaydon from The Commonwealth Scientific and Industrial Research Organisation (CSIRO).

Running the simulation requires for Singularity 3.0 or higher to be installed. Then the user can send the bash files to the job-scheduler, after ensuring the file paths link to the correct folders. The bash scripts are designed to run on CIMMYT's HPC system and may have to be adapted to be run elsewhere and/or other users. More significant adjustments may have to be made if another job scheduler than slurm is used. The scenarios are designed as one run per batch file. Alternatively, scenarios may be included in the Campaign file as explained on the psIMS github website.


Simulations outputs have been copied into the analytics folder and can be run out of the box with working directory set into the analytics folder. Simulation naming scheme can be checked in the config/batch_files folder. Helper scripts to create input data in the config or analytics files will be added shortly.

This work has been part of the Cereal Systems Initiative for South Asia (CSISA, [www.csisa.org](url)) project supported by the United States Agency for International Development (USAID) and the Bill and Melinda Gates Foundation (BMGF). The content and opinions expressed in this work are those of the authors and do not necessarily reflect the views of USAID or the BMGF.

For question please contact Anton Urfels (anton.urfels@wur.nl).
