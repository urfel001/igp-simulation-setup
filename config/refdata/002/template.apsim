<folder version="32" name="simulations">
  #foreach ($simulation in $experiments)
        <simulation name="Generic">
                <memo name="LOG">$simulation.log</memo>
                <clock>
                        <start_date type="date" description="Enter the start date of the simulation">$simulation.startDate</start_date>
                        <end_date type="date" description="Enter the end date of the simulation">$simulation.endDate</end_date>
                </clock>
                <metfile name="met">
                        <filename name="filename" input="yes">${simulation.weather.file}.met</filename>
                </metfile>
                <summaryfile />
                <area name="paddock">
                        <soil name="Soil">
                                <Comment type="multiedit" description="Comments">ICASA Classification: $simulation.soil.classification</Comment>
                                <ASC_Order description="Australian Soil Classification Order" />
                                <ASC_Sub-order description="Australian Soil Classification Sub-Order" />
                                <SoilType description="Soil description"/>
                                <LocalName/>
                                <Site>$simulation.soil.site</Site>
                                <NearestTown description="Nearest town"/>
                                <Region/>
                                <State />
                                <Country />
                                <NaturalVegetation description="Natural vegetation"/>
                                <ApsoilNumber description="Apsoil number" />
                                <Latitude description="Latitude (WGS84)">$simulation.soil.latitude</Latitude>
                                <Longitude>$simulation.soil.longitude</Longitude>
                                <LocationAccuracy description="Location accuracy" />
                                <DataSource type="multiedit" description="Data source">$simulation.soil.source</DataSource>
                                <Comments />
                                <Water>
#foreach ($layer in $simulation.soil.layers)
                                        <Layer>
                                                <Thickness units="mm">$layer.thickness</Thickness>
                                                <KS units="mm/day" />
                                                <BD units="g/cc">$layer.bulkDensity</BD>
                                                <AirDry units="mm/mm">$layer.airDry</AirDry>
                                                <LL15 units="mm/mm">$layer.lowerLimit</LL15>
                                                <DUL units="mm/mm">$layer.drainedUpperLimit</DUL>
                                                <SAT units="mm/mm">$layer.saturation</SAT>
                                        </Layer>
#end
                                        <SoilCrop name="wheat">
#foreach ($layer in $simulation.soil.layers)
                                                <Layer>
                                                        <Thickness units="mm">$layer.thickness</Thickness>
                                                        <LL units="mm/mm">$layer.lowerLimit</LL>
                                                        <KL units="/day">$layer.kl</KL>
                                                        <XF units="0-1">1</XF>
                                                </Layer>
#end
                                        </SoilCrop>
                                </Water>
                                <SoilWat>
                                        <SummerCona>3.5</SummerCona>
                                        <SummerU>$simulation.soil.u</SummerU>
               <SummerDate>1-Nov</SummerDate>
                                        <WinterCona>3.5</WinterCona>
                                        <WinterU>$simulation.soil.u</WinterU>
               <WinterDate>1-Apr</WinterDate>
                                        <DiffusConst>$simulation.soil.diffusConst</DiffusConst>
                                        <DiffusSlope>$simulation.soil.diffusSlope</DiffusSlope>
                                        <Salb>$simulation.soil.salb</Salb>
                                        <Cn2Bare>$simulation.soil.cn2bare</Cn2Bare>
                                        <CnRed>20</CnRed>
                                        <CnCov>0.8</CnCov>
                                        <Slope>
                                        </Slope>
                                        <DischargeWidth>
                                        </DischargeWidth>
                                        <CatchmentArea>
                                        </CatchmentArea>
                                        <MaxPond>
                                        </MaxPond>
#foreach ($layer in $simulation.soil.layers)
                                        <Layer>
                                                <Thickness units="mm">$layer.thickness</Thickness>
                                                <SWCON units="0-1">0.3</SWCON>
                                                <MWCON units="0-1">0.3</MWCON>
                                                <KLAT units="mm/d">2</KLAT>
                                        </Layer>
#end
                                </SoilWat>
                                <SoilOrganicMatter>
                                        <RootCn>45</RootCn>
                                        <RootWt>500</RootWt>
                                        <SoilCn>14.5</SoilCn>
                                        <EnrACoeff>7.4</EnrACoeff>
                                        <EnrBCoeff>0.2</EnrBCoeff>
#foreach ($layer in $simulation.soil.layers)
                                        <Layer>
                                                <Thickness units="mm">$layer.thickness</Thickness>
                                                <OC units="Total %">#if( $layer.organicCarbon == 0 ) 0.1 #else $layer.organicCarbon #end</OC>
                                                <FBiom units="0-1">$layer.fbiom</FBiom>
                                                <FInert units="0-1">$layer.finert</FInert>
                                        </Layer>
#end
                                </SoilOrganicMatter>
                                <Analysis>
#foreach ($layer in $simulation.soil.layers)
                                        <Layer>
                                                <Thickness units="mm">$layer.thickness</Thickness>
                                                <Rocks units="%" />
                                                <Texture/>
                                                <MunsellColour/>
                                                <EC units="1:5 dS/m"/>
                                                <PH units="1:5 water">$layer.ph</PH>
                                                <CL units="mg/kg" />
                                                <Boron units="Hot water mg/kg" />
                                                <CEC units="cmol+/kg" />
                                                <Ca units="cmol+/kg" />
                                                <Mg units="cmol+/kg" />
                                                <Na units="cmol+/kg" />
                                                <K units="cmol+/kg" />
                                                <ESP units="%" />
                                                <Mn units="mg/kg" />
                                                <Al units="cmol+/kg" />
                                                <ParticleSizeSand units="%" />
                                                <ParticleSizeSilt units="%" />
                                                <ParticleSizeClay units="%" />
                                        </Layer>
#end
                                </Analysis>
                                <Sample name="Initial nitrogen">
                                        <Date type="date" description="Sample date:">$simulation.initialCondition.date</Date>
#foreach ($initialLayer in $simulation.initialCondition.soilLayers)
                                        <Layer>
                                                <Thickness units="mm">$initialLayer.thickness</Thickness>
                                                <NO3 units="ppm">$initialLayer.no3</NO3>
                                                <NH4 units="ppm">$initialLayer.nh4</NH4>
                                        </Layer>
#end
                                </Sample>
                              <InitialWater>
                                      <FractionFull>$simulation.initialCondition.water_fraction_full</FractionFull>
                                      <DepthWetSoil>NaN</DepthWetSoil>
                                      <PercentMethod>EvenlyDistributed</PercentMethod>
                                      <RelativeTo>ll15</RelativeTo>
                              </InitialWater>
                        </soil>
                        <surfaceom name="SurfaceOrganicMatter">
                                <PoolName type="text" description="Organic Matter pool name">OrganicMatter</PoolName>
                                <type type="list" listvalues="bambatsi,barley,base_type,broccoli,camaldulensis,canola,centro,chickpea,chikenmanure_base,cm,cmA,cmB,constants,cotton,cowpea,danthonia,fababean,fieldpea,fym,gbean,globulus,goatmanure,grandis,grass,horsegram,inert,lablab,lentil,lucerne,lupin,maize,manB,manure,medic,millet,mucuna,nativepasture,navybean,oats,orobanche,peanut,pigeonpea,potato,rice,sorghum,soybean,stylo,sugar,sunflower,sweetcorn,sweetsorghum,tillage,tithonia,vetch,weed,wheat" description="Organic Matter type">$simulation.initialCondition.residue_type</type>
                                <mass type="text" description="Initial surface residue (kg/ha)">$simulation.initialCondition.residueWeight</mass>
                                <cnr type="text" description="C:N ratio of initial residue">$simulation.initialCondition.cnr</cnr>
                                <standing_fraction type="text" description="Fraction of residue standing">$simulation.initialCondition.standing_fraction</standing_fraction>
                        </surfaceom>
                        <fertiliser />
      <irrigation name="Irrigation">
        <automatic_irrigation type="list" listvalues="on,off" description="Automatic irrigation">off</automatic_irrigation>
        <asw_depth type="text" description="Depth to which ASW is calculated. (mm)">$simulation.irrigation.asw_depth</asw_depth>
        <crit_fr_asw type="text" description="Fraction of ASW below which irrigation is applied (0-1.0)">$simulation.irrigation.crit_fr_asw</crit_fr_asw>
        <irrigation_efficiency type="text" description="Efficiency of the irrigation. (0-1.0)">$simulation.irrigation.efficiency</irrigation_efficiency>
        <irrigation_allocation type="list" listvalues="on,off" description="Allocation limits">$simulation.irrigation.allocation_limits</irrigation_allocation>
        <allocation type="text" description="Allocation in mm">$simulation.irrigation.allocation</allocation>
        <default_no3_conc type="text" description="Nitrate concentration (ppm N)">$simulation.irrigation.default_no3_conc</default_no3_conc>
        <default_nh4_conc type="text" description="Ammonium concentration (ppm N)">$simulation.irrigation.default_nh4_conc</default_nh4_conc>
        <default_cl_conc type="text" description="Chloride concentration (ppm Cl)">$simulation.irrigation.default_cl_conc</default_cl_conc>
      </irrigation>
        <rice>
          <ini>
            <filename input="yes">rice.xml</filename>
          </ini>
        </rice>
        <wheat>
          <ini>
            <filename input="yes">wheat.xml</filename>
          </ini>
        </wheat>
        <folder name="Manager folder">
          <manager name="Manager">
            <script>
              <text>

r_dah = 0
rice_stage = 'out'
count = 0
irrig_amount = 0
nursery_days = 0
clay_perc = $simulation.clayp
p_all = $simulation.sample
p_1982 = $simulation.p1982
p_1983 = $simulation.p1983
p_1984 = $simulation.p1984
p_1985 = $simulation.p1985
p_1986 = $simulation.p1986
p_1987 = $simulation.p1987
p_1988 = $simulation.p1988
p_1989 = $simulation.p1989
p_1990 = $simulation.p1990
p_1991 = $simulation.p1991
p_1992 = $simulation.p1992
p_1993 = $simulation.p1993
p_1994 = $simulation.p1994
p_1995 = $simulation.p1995
p_1996 = $simulation.p1996
p_1997 = $simulation.p1997
p_1998 = $simulation.p1998
p_1999 = $simulation.p1999
p_2000 = $simulation.p2000
p_2001 = $simulation.p2001
p_2002 = $simulation.p2002
p_2003 = $simulation.p2003
p_2004 = $simulation.p2004
p_2005 = $simulation.p2005
p_2006 = $simulation.p2006
p_2007 = $simulation.p2007
p_2008 = $simulation.p2008
p_2009 = $simulation.p2009
p_2010 = $simulation.p2010
p_2011 = $simulation.p2011
p_2012 = $simulation.p2012
p_2013 = $simulation.p2013
p_2014 = $simulation.p2014
p_2015 = $simulation.p2015
pzone = $simulation.pzone



              </text>
              <event>Init</event>
            </script>
            <script>
              <text>

!********* Soil water variable calculations********************

!Saturation content  
saturation_deficit = sat_dep(1)- sw_dep(1) + sat_dep(2)- sw_dep(2) + sat_dep(3)- sw_dep(3)+ sat_dep(4)- sw_dep(4)+ sat_dep(5) - sw_dep(5)

!Soil water contents 
current_dep = sw_dep(1) + sw_dep(2) + sw_dep(3) + sw_dep(4) + sw_dep(5) 

sat_dep50 = sat_dep(1) + sat_dep(2) + sat_dep(3) + sat_dep(4) + sat_dep(5) 
fc_dep50 = dul_dep(1) + dul_dep(2) + dul_dep(3) +  dul_dep(4) + dul_dep(5)

fc_perc_fill = current_dep / fc_dep50
sat_perc_fill = current_dep / sat_dep50
sat_deficit_depth = sat_dep50 - current_dep 



!****************************** Soil Cracking **********************************************

sw_crack = (sw(1) + sw(2) + sw(3) + sw(4) + sw(5) )/ 5

Crack_depth = 93.4*((-0.042+(0.0037*(Clay_perc)))*(Clay_perc/100)/sw_crack)^0.56

if (crack_depth > 15 and AWD_day > 0 and cracking = 0) then
    cracking = 1
    !'Soil Water' set ks = 80 80 80 30 60 80 80 80
    cracked_before = 1
endif


if (fc_perc_fill > 1 and cracking = 1) then
    sat_days = sat_days + 1
else
    sat_days = 0
endif

if (sat_days > 3 and cracked_before = 1) then
    !'Soil Water' set ks = 80 80 80 15 60 80 80 80
    cracking = 0
endif



!***************************** Pdate selection for each year **********************************************


if (Year = 1981) then 
pdate = p_1981
endif
if (Year = 1982) then 
pdate = p_1982
endif
if (Year = 1983) then 
pdate = p_1983
endif
if (Year = 1984) then 
pdate = p_1984
endif
if (Year = 1985) then 
pdate = p_1985
endif
if (Year = 1986) then 
pdate = p_1986
endif
if (Year = 1987) then 
pdate = p_1987
endif
if (Year = 1988) then 
pdate = p_1988
endif
if (Year = 1989) then 
pdate = p_1989
endif
if (Year = 1990) then 
pdate = p_1990
endif
if (Year = 1991) then 
pdate = p_1991
endif
if (Year = 1992) then 
pdate = p_1992
endif
if (Year = 1993) then 
pdate = p_1993
endif
if (Year = 1994) then 
pdate = p_1994
endif
if (Year = 1995) then 
pdate = p_1995
endif
if (Year = 1996) then 
pdate = p_1996
endif
if (Year = 1997) then 
pdate = p_1997
endif
if (Year = 1998) then 
pdate = p_1998
endif
if (Year = 1999) then 
pdate = p_1999
endif
if (Year = 2000) then 
pdate = p_2000
endif
if (Year = 2001) then 
pdate = p_2001
endif
if (Year = 2002) then 
pdate = p_2002
endif
if (Year = 2003) then 
pdate = p_2003
endif
if (Year = 2004) then 
pdate = p_2004
endif
if (Year = 2005) then 
pdate = p_2005
endif
if (Year = 2006) then 
pdate = p_2006
endif
if (Year = 2007) then 
pdate = p_2007
endif
if (Year = 2008) then 
pdate = p_2008
endif
if (Year = 2009) then 
pdate = p_2009
endif
if (Year = 2010) then 
pdate = p_2010
endif
if (Year = 2011) then 
pdate = p_2011
endif
if (Year = 2012) then 
pdate = p_2012
endif
if (Year = 2013) then 
pdate = p_2013
endif
if (Year = 2014) then 
pdate = p_2014
endif
if (Year = 2015) then 
pdate = p_2015
endif



!**********************************Rice Planting ****************************************************
     
if (paddock_is_fallow() = 1  and Day = pzone) then                         
    rice sow cultivar = mtu7029, establishment = transplant, sbdur = 20, nplh = 2, nh = 25, nplsb = 1000
    fertiliser apply amount = 150, type= urea_N            !Inital fertilizer
    nursery_days = 0
    puddle_sw = sw_content
    rice_grainfilling_days = 0
    rice_irrig_count = 0
    rice_flowering_days = 0
    rice_repr_days = 0
    rice_veg_days = 0
    puddle_irrig = saturation_deficit * 1.5
    irrigation apply amount = sat_deficit_depth
    'Soil Water' set ks = 80 80 80 4 60 80 80 80
    rice_sow_date = day
    'Soil Water' set max_pond = 150
    endif
endif


!*!!!!!!!!!!!!!!*** !*!!!!!!!!!!!!!!*** counting days after transplanting
     
if rice.plant_status = 'alive' and cropsta &gt;= 3 then
    datp = 1 + datp
    rice_rain = rain + rice_rain
endif

!!!!!!!!!!!!!!!!!!! counting transpiration deficit
trans_deficit = rice.trc - rice.trw 



!**************** FERTILISER  and IRRIGATION For Rice *******************************************
     !****** Counting AWD days**************     
if rice.plant_status = 'alive' and cropsta &gt;= 3 and pond_depth &lt;= 0 then  !cropsta =3 means that crop has emerged out of soil. &lt;3 is bare soil
    AWD_day = 1 + AWD_day
endif

if rice.plant_status = 'alive' and datp &gt; 1  and AWD_day &gt; 4 and dvs &lt; 1.800 then  !dvs=2 is maturity, so irrigate until maturity
    irrig_amount = 50 + saturation_deficit
    irrigation apply amount = irrig_amount
    rice_irrig_count = rice_irrig_count + 1
    AWD_day = 0 
endif

!**************** FERTILISER  *******************************************

if datp = 20 then
    fertiliser apply amount = 50, type=urea_n
endif
     
if datp = 41 then
    fertiliser apply amount = 50, type=urea_n
endif


!*********************************End Rice*****************************************************


if rice.plant_status = 'dead' then                                                                                                       ! Harvest Rice crop
    rice  end_crop
    datp = 0
    d_day = 0
    AWD_day = 0
    rice_harvest_date = day
    cracking = 0
    cracked_before = 0
    r_dah = 1 !set harvest days to 1
endif


! *************************** Days after rice harvest ********************************

if (r_dah &lt;&gt; 0) then
    r_dah = r_dah + 1
endif


!************************Wheat sowing**************************************************************

if r_dah = 10 then
    'SurfaceOrganicMatter' tillage type = burn
endif
      
if r_dah = 15 then
    'irrigation' apply amount = 75
endif
      
if r_dah = 20 then
    'Soil water' tillage_type = disc
endif 

if r_dah = 22 then
    'Soil water' tillage_type = disc
endif

if r_dah= 24 then
    'Soil water' tillage_type = disc
endif

if (paddock_is_fallow() = 1 and FallowIn &lt;&gt; 'yes' and (NextCrop = 0 or NextCrop = 'wheat')) then
    if (r_dah  = 25) then   
        !if (rain[3] &gt;= 0 AND esw &gt;= 200) OR
        (r_dah  = 25) THEN
        ChooseNextCrop = 'yes'   ! for rotations
        wheat sow plants = 100, sowing_depth = 30, cultivar = pbw343_short, row_spacing = 250, crop_class = plant
        wsow_irrig = (sat_dep - sw_dep) !this takes all
        fertiliser apply amount = 130, type = urea
        wht_irrig_count = 0
        'Soil Water' set ks = 80 80 80 30 60 80 80 80
        wheat_sow_date = day
    endif
        if today = date('8-jan') then
            ChooseNextCrop = 'yes'
        endif
    endif
endif


! ************************Wheat Fertilize  Irrigate at  Twenty days after sowing******************

if no3(1) + no3(2) &lt; 20 then
    fertiliser apply amount = 30, depth = 50, type = no3_N
endif

if wheat.daysaftersowing = 20 then        
    fertiliser apply amount = 30, depth = 50, type = urea_N         
endif

  
if wheat.daysaftersowing = 41 then        
    fertiliser apply amount = 30, depth = 50, type = urea_N         
endif

!**************** irrigation based on 50% SWD of 0-60cm soil profile  *******************************************

if wheat.plant_status = 'alive' and wheat.sw_supply &lt; wheat.sw_demand and Zadok_stage &lt; 80 then
    irrigation apply amount = saturation_deficit 
    wht_irrig_count = wht_irrig_count + 1
endif


!****************************** End and Harvest Wheat Crop ********************************
if wheat.stagename = 'harvest_ripe' or wheat.plant_status = 'dead' or today = date('20-may')  then
    wheat_harvest_date = day
    wheat harvest
    wheat end_crop
endif




</text>
<event>start_of_day</event>
</script>
<script>
<text>
if eff_rain &gt; 0 then
    Pe = eff_rain
    else
    Pe = 0
endif
</text>
              <event>end_of_day</event>
            </script>
          </manager>
    </folder>
        <pond>
          <algae_present type="text" description="is there algae present in this pond (0 or 1)">0</algae_present>
          <CEC type="text" description="surface soil cation exchange capacity">30.0</CEC>
        </pond>      
<tracker>
           <variable>value of rice.wrr on rice.harvesting as rice_yield</variable>
          <variable>value of rice.wagt on rice.harvesting as rice_biomass</variable>
          <variable>average of rice.lrstrs on end_of_day from rice.sowing to rice.harvesting as avg_lrstrs</variable>
          <variable>sum of Pe on end_of_day from rice.sowing to rice.harvesting as rice_effrain</variable>
          <variable>sum of trw on end_of_day from rice.sowing to rice.harvesting as rice_trans</variable>
          <variable>value of wheat.yield on wheat.harvesting as wheat_yield</variable>
          <variable>sum of irrigation on end_of_day from rice.sowing to rice.harvesting as rice_irrig</variable>
          <variable>value of runoff on rice.sowing as tpr_runoff</variable>
          <variable>value of drain on rice.sowing as tpr_drain</variable>
          <variable>sum of runoff on end_of_day from rice.sowing to rice.harvesting as rice_runoff</variable>
          <variable>sum of drain on end_of_day from rice.sowing to rice.harvesting as rice_drain</variable>
          <variable>sum of infiltration on end_of_day from rice.sowing to rice.harvesting as rice_infil</variable>
          <variable>sum of eff_rain on end_of_day from rice.sowing to rice.harvesting as rice_rain</variable>
          <variable>average of rice.sf1 on end_of_day from rice.sowing to rice.harvesting as avg_sf1</variable>
          <variable>average of rice.sf2 on end_of_day from rice.sowing to rice.harvesting as avg_sf2</variable>
          <variable>average of rice.pcew on end_of_day from rice.sowing to rice.harvesting as avg_pcew</variable>
          <variable>average of rice.ldstrs on end_of_day from rice.sowing to rice.harvesting as avg_ldstrs</variable>
          <variable>average of rice.lestrs on end_of_day from rice.sowing to rice.harvesting as avg_lestrs</variable>
          <variable>average of wheat.sw_stress_photo on end_of_day from wheat.sowing to wheat.harvesting as avg_sw_stress_photo</variable>
          <variable>average of wheat.temp_stress_photo on end_of_day from wheat.sowing to wheat.harvesting as avg_temp_stress_photo</variable>
          <variable>average of wheat.dlt_slai_heat on end_of_day from wheat.sowing to wheat.harvesting as avg_dlt_slai_heat</variable>
          <variable>average of wheat.dlt_slai_frost on end_of_day from wheat.sowing to wheat.harvesting as avg_dlt_slai_frost</variable>
          <variable>sum of runoff on end_of_day from rice.sowing to wheat.harvesting as wheat_runoff</variable>
          <variable>sum of drain on end_of_day from wheat.sowing to wheat.harvesting as wheat_drain</variable>
          <variable>sum of infiltration on end_of_day from wheat.sowing to wheat.harvesting as wheat_infil</variable>
          <variable>sum of Pe on end_of_day from wheat.sowing to wheat.harvesting as wheat_effrain</variable>
          <variable>sum of es on end_of_day from wheat.sowing to wheat.harvesting as wheat_evapo</variable>
          <variable>sum of es on end_of_day from rice.sowing to rice.harvesting as rice_evapo</variable>
          <variable>sum of pond.pond_evap on end_of_day from rice.sowing to rice.harvesting as rice_pondevapo</variable>
          <variable>sum of wheat.ep on end_of_day from wheat.sowing to wheat.harvesting as wheat_trans</variable>
          <variable>sum of rice.sw_demand on end_of_day from rice.sowing to rice.harvesting as rice_swDemand</variable>
          <variable>sum of trans_deficit on end_of_day from rice.sowing to rice.harvesting as rice_transDeficit</variable>
          <variable>sum of irrigation on end_of_day from wheat.sowing to wheat.harvesting as wheat_irrig</variable>
          <variable>average of MinT on end_of_day from wheat.sowing to wheat.harvesting as wheat_avgMinT</variable>
          <variable>average of MaxT on end_of_day from wheat.sowing to wheat.harvesting as wheat_avgMaxT</variable>
          <variable>average of Radn on end_of_day from wheat.sowing to wheat.harvesting as wheat_avgRadn</variable>
          <variable>average of vp on end_of_day from wheat.sowing to wheat.harvesting as wheat_avgVp</variable>
          <variable>average of MinT on end_of_day from rice.sowing to rice.harvesting as rice_avgMinT</variable>
          <variable>average of MaxT on end_of_day from rice.sowing to rice.harvesting as rice_avgMaxT</variable>
          <variable>average of Radn on end_of_day from rice.sowing to rice.harvesting as rice_avgRadn</variable>
          <variable>average of vp on end_of_day from rice.sowing to rice.harvesting as rice_avgVp</variable>
          <variable>value of rice.dae on rice.harvesting as rice_duration</variable>
          <variable>count of tick from wheat.sowing to now as wheat_duration</variable>
</tracker>
<outputfile>
<filename output="yes">Generic.out</filename>
<title>AgMIP</title>
            <variables name="Variables">
            <variable>year as Dates</variable>
            <variable>rice_yield</variable>
            <variable>rice_biomass</variable>
            <variable>yield as wheat_yield</variable>
            <variable>biomass as wheat_biomass</variable>
            <variable>rice_effrain</variable>
            <variable>rice_trans</variable>
            <variable>puddle_irrig</variable>
            <variable>wsow_irrig</variable>
            <variable>avg_lrstrs</variable>
            <variable>avg_ldstrs</variable>
            <variable>avg_lestrs</variable>
            <variable>avg_pcew</variable>
            <variable>avg_sf1</variable>
            <variable>avg_sf2</variable>
            <variable>avg_sw_stress_photo</variable>
            <variable>avg_temp_stress_photo</variable>
            <variable>avg_dlt_slai_frost</variable>
            <variable>avg_dlt_slai_heat</variable>
            <variable>wheat_sow_date</variable>
            <variable>rice_sow_date</variable>
            <variable>wheat_effrain</variable>
            <variable>wheat_evapo</variable>
            <variable>rice_evapo</variable>
            <variable>wheat_trans</variable>
            <variable>rice_pondevapo</variable>
            <variable>rice_swDemand</variable>
            <variable>rice_transDeficit</variable>
            <variable>wheat_harvest_date</variable>
            <variable>rice_harvest_date</variable>
            <variable>rice_irrig</variable>
            <variable>wheat_irrig</variable>
            <variable>eff_rain</variable>
            <variable>Rain</variable>
            <variable>Pe</variable>
            <variable>wheat_avgMinT</variable>
            <variable>wheat_avgMaxT</variable>
            <variable>wheat_avgRadn</variable>
            <variable>wheat_avgVp</variable>
            <variable>rice_avgMinT</variable>
            <variable>rice_avgMaxT</variable>
            <variable>rice_avgRadn</variable>
            <variable>rice_avgVp</variable>
            <variable>rice_duration</variable>
            <variable>wheat_duration</variable>
            <variable>wht_irrig_count</variable>
            <variable>rice_irrig_count</variable>
            <variable>wheat_drain</variable>
            <variable>wheat_infil</variable>
            <variable>wheat_runoff</variable>
            <variable>rice_drain</variable>
            <variable>rice_infil</variable>
            <variable>rice_runoff</variable>
            </variables>
            <events name="Reporting Frequency">
                <event>wheat.harvesting</event>
            </events>
        </outputfile>
                </area>
    </simulation>
  #end
</folder>

