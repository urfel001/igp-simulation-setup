#!/bin/bash
#SBATCH -p cimmythpc 
#SBATCH --job-name="PSIMS_P2_era5_localSSD"
#SBATCH -n 90 #number of cores
#SBATCH --mem=400GB
#SBATCH -o slurm.%N.%j.out 
#SBATCH -e slurm.%N.%j.err
#SBATCH --mail-type=BEGIN,END,FAIL 
#SBATCH --mail-user=a.urfels@cgiar.org 
#SBATCH -t 1-0:00
# remove old files
rm -frv /local_ssd/aurfels/*
#copy required files from home
cp -Rv /home/aurfels/planting-sims/ /local_ssd/aurfels/
cp -Rv /home/aurfels/planting-data/ /local_ssd/aurfels/
# cd into run directory and run simulations
cd /local_ssd/aurfels/planting-sims/psims/
singularity exec /local_ssd/aurfels/planting-data/base_opt.sif ./psims -s local -p /local_ssd/aurfels/planting-sims/config/params/004 -c /local_ssd/aurfels/planting-sims/config/campaigns/campaign_R/ -t /local_ssd/aurfels/planting-sims/config/tilelists/tileList.IGP
#copy results back - need to repalce path for each scenario
now=$(date +"%m_%d_%Y_%H_%M_%S")    
mkdir -v /home/aurfels/output/004-${now} 
mv -v run* /home/aurfels/output/004-${now}
# remove all files from local ssd - to be released after the rest previous parts including the copying is tested
# rm -R /local_ssd/aurfels/*  


